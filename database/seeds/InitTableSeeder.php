<?php

use Illuminate\Database\Seeder;
use App\Models\Banner;
use App\Models\Category;
use App\Models\Product;

class InitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Seeder Banners
        $banner         = new Banner;
        $banner->title  = "Sate Buah";
        $banner->save();
        $banner->image()->create([
            'url' => 'https://www.ayobandung.com/images-bandung/post/articles/2019/01/17/43407/vegetable-skewer-3317060_640.jpg'
        ]);
        $banner = new Banner;
        $banner->title = "Soto Mie";
        $banner->save();
        $banner->image()->create([
            'url' => 'https://cdn-asset.hipwee.com/wp-content/uploads/2017/01/hipwee-97617-1000xauto-mie-warung--750x422.jpg'
        ]);

        //Seeder Category and Product
        $category           = new Category;
        $category->title    = 'Buah';
        $category->save();
        $category->icon()->create([
            'url' => 'https://i0.wp.com/img-global.cpcdn.com/003_recipes/03f0e7d2e9e66389/150x100cq50/photo.jpg'
        ]);
        
        $product        = new Product;
        $product->title = 'Sate Buah Segar';
        $product->price = 15000;
        $product->is_halal = rand(0, 1);
        $product->category_id = $category->id;
        $product->save();
        $product->assets()->create([
            'url' => 'https://cdn.hellosehat.com/wp-content/uploads/2018/11/shutterstock_1148354840.jpg'
        ]);
        $product->assets()->create([
            'url' => 'https://cdn-asset.hipwee.com/wp-content/uploads/2017/01/hipwee-97617-1000xauto-mie-warung--750x422.jpg'
        ]);

        $product        = new Product;
        $product->title = 'Soup Buah Segar';
        $product->price = 10000;
        $product->is_halal = rand(0, 1);
        $product->category_id = $category->id;
        $product->save();
        $product->assets()->create([
            'url' => 'https://cdn-asset.hipwee.com/wp-content/uploads/2017/01/hipwee-97617-1000xauto-mie-warung--750x422.jpg'
        ]);
        $product->assets()->create([
            'url' => 'https://cdn.hellosehat.com/wp-content/uploads/2018/11/shutterstock_1148354840.jpg'
        ]);

        $category           = new Category;
        $category->title    = 'Mie';
        $category->save();
        $category->icon()->create([
            'url' => 'http://infokuliner.com/wp-content/uploads/2015/05/76.-nasi-goreng-gila_ykx60s-150x100.jpg'
        ]);

        $product        = new Product;
        $product->title = 'Soto Mie Jogja';
        $product->price = 12000;
        $product->is_halal = rand(0, 1);
        $product->category_id = $category->id;
        $product->save();
        $product->assets()->create([
            'url' => 'https://cdn.hellosehat.com/wp-content/uploads/2017/05/4-Sumber-Makanan-yang-Wajib-Ada-Selama-Bulan-Puasa.jpg'
        ]);
        $product->assets()->create([
            'url' => 'https://www.ayobandung.com/images-bandung/post/articles/2019/01/17/43407/vegetable-skewer-3317060_640.jpg'
        ]);
    }
}
