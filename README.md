## How to install
- Clone Repository
- run "composer install"
- setting up the .env file
- run "php artisan migrate"
- run "composer dump-autoload" (optional but recomended)
- run "php artisan db:seed --class=InitTableSeeder"
- run "php artisan server"

### Get Product
- localhost:8000/api/v1/product (all product)
- localhost:8000/api/v1/product/{id} (detail product)
- localhost:8000/api/v1/product?page=1&show=1 (page 1 and show 1 data)
- localhost:8000/api/v1/product?page=1&show=1&category=2 (page 1 and show 1 data and category_id 2)
- localhost:8000/api/v1/product?page=1&show=1&category=2&search=segar (page 1 and show 1 data and category_id 2 and title keyword "segar")

### Get Banner
- localhost:8000/api/v1/banner (all banner)
- localhost:8000/api/v1/banner/{id} (detail banner)

### Get Category
- localhost:8000/api/v1/category (all category)
