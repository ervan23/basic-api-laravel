<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $hidden   = ['created_at', 'updated_at'];
    protected $appends  = ['image'];
    
    public function getImageAttribute() {
        return $this->attributes['image'] = 
        $this->morphOne(\App\Models\Attachment::class, 'attachable')->first();
    }

    public function image() {
        return $this->morphOne(\App\Models\Attachment::class, 'attachable');
    }

}
