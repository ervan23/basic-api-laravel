<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $hidden   = ['created_at', 'updated_at'];
    protected $appends  = ['cover'];

    public function getCoverAttribute() {
        return $this->attributes['images'] = $this->assets[0];
    }

    public function assets() {
        return $this->morphMany(\App\Models\Attachment::class, 'attachable');
    }

    public function category() {
        return $this->belongsTo(\App\Models\Category::class);
    }

}
