<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $hidden   = ['created_at', 'updated_at'];
    protected $appends  = ['icon'];
    
    public function getIconAttribute() {
        return $this->attributes['icon'] = 
        $this->morphOne(\App\Models\Attachment::class, 'attachable')->first();
    }

}
