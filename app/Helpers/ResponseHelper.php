<?php

namespace App\Helpers;

final class ResponseHelper {

    static function success ($message, $payload = [], $code = 200) {
        return response()->json([
            'error'     => false,
            'message'   => $message,
            'payload'   => $payload,
        ], $code);
    }

    static function error ($message, $payload = null, $code = 500) {
        return response()->json([
            'error'     => true,
            'message'   => $message,
            'payload'   => $payload,
        ], $code);
    }
}