<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    
    public function get (Request $req, $product = null) {
        $validator = \Validator::make($req->all(), [
            'page'      => 'required_with:show|integer|min:1',
            'show'      => 'sometimes:page|integer',
            'category'  => 'sometimes',
            'search'    => 'sometimes'
        ]);

        if ($validator->fails()) {
            return \Json::error($validator->errors()->first(), null, 400);
        }

        try {
            if ($product) {
                return \Json::success('Product retrieved', $product);
            }

            $products = Product::query();

            if ($req->has('category')) {
                $products->where('category_id', $req->category);
            }

            if ($req->has('search')) {
                $products->where('title', 'like', "%{$req->search}%");
            }

            //Get total data
            $total = $products->get()->count();
            //Paginate data
            $take = $req->has('show') ? $req->show : 10;
            $skip = $req->has('page') ? $take * ($req->page - 1) : 0;
            $products = $products->take($take)->skip($skip)->get();

            return \Json::success('List products retrieved', [
                'data'  => $products,
                'total' => $total
            ]);
        } catch (\Exception $e) {
            return \Json::error($e->getMessage());
        }
    }

}
