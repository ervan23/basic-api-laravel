<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;

class BannerController extends Controller
{
    public function get(Request $req, Banner $banner = null) {
        try {
            if ($banner) {
                return \Json::success('Banner retrieved', $banner);
            }

            $banners = Banner::all();
            return \Json::success('Banner list retrieved', $banners);
        } catch (\Exception $e) {
            return \Json::error($e->getMessage());
        }
    }
}
