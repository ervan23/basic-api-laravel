<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function get (Request $req) {
        try {
            $categories = Category::all();
            return \Json::success('Categories list retrieved', $categories);
        } catch (\Exception $e) {
            return \Json::error($e->getMessage());
        }
    }
}
